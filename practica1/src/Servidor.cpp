#include "glut.h"
#include "MundoServidor.h"
//#include "signal.h"

CMundo mundo;


void OnDraw(void); 
void OnTimer(int value); 
void OnKeyboardDown(unsigned char key, int x, int y); 
/*int aux1;//Esta variable sirve para tratar la alarma 
void tratar_alarma(int)
{	
	extern int aux1;
	aux1=0;
}*/

int main(int argc,char* argv[])
{
	/*struct sigaction act;
	act.sa_handler=&tratar_alarma;
	act.sa_flags=SA_RESTART;
	sigaction(SIGALRM,&act,NULL);*/

	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("Servidor");


	glutDisplayFunc(OnDraw);

	glutTimerFunc(25,OnTimer,0);
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	mundo.InitGL();


	glutMainLoop();	
	

	return 0;   
}

void OnDraw(void)
{
	mundo.OnDraw();
}
void OnTimer(int value)
{
	mundo.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}
void OnKeyboardDown(unsigned char key, int x, int y)
{
	mundo.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}
